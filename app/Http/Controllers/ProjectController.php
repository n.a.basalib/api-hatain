<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class ProjectController extends Controller
{
    public function index(Request $reqest)
    {
        $projects = Project::get();
		return ResponseFactory::resJson($projects, 'Projects List.');
    }

    public function store(Project $project, Request $reqest)
    {
		try{
			$project->name = $reqest->name;
			$project->description = $reqest->description;
			$project->iteration = $reqest->iteration;
			$project->total = $reqest->total;
			$project->payment_period = $reqest->payment_period;
			$project->fees = $reqest->fees;
			$project->status = $reqest->status;
			$project->save();
			return ResponseFactory::resJson(null, 'Project Add Successfully.');
		}catch(Exception $e){
			return ResponseFactory::errorJson(null, 'Oops, something went wrong');
		}
    }

    public function update($id, Request $reqest)
    {
		try{
			$project = Project::find($id);
			$project->name = $reqest->name;
			$project->description = $reqest->description;
			$project->iteration = $reqest->iteration;
			$project->total = $reqest->total;
            $project->payment_period = $reqest->payment_period;
			$project->fees = $reqest->fees;
            $project->status = $reqest->status;
			$project->save();
			return ResponseFactory::resJson(null, 'Project Updated Successfully.');
		}catch(Exception $e){
			return ResponseFactory::errorJson(null, "Oops, something went wrong.");			
		}
    }

	public function delete($id)
    {
		try{
			Project::find($id)->delete();
			return ResponseFactory::resJson(null, 'Project Deleted Successfully.');
		}catch(Exception $e){
			return ResponseFactory::errorJson(null, "Oops, something went wrong.");
		}
    }
}
