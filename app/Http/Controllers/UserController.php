<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::allUsers();

        return ResponseFactory::resJson($users, 'Users.');
    }
}
