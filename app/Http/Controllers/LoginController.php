<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        if(!Auth::attempt(['email'=> $request->email, 'password'=> $request->password])){
            return ResponseFactory::errorJson(null, "The provided credentials do not match our records.");
        }

        // remember me

        $user = $request->user();
        $token = $user->createToken('user-token');
        
        $data = ['user' => $user,'token' => $token];
        return ResponseFactory::resJson($data, 'logged in');
    }

    public function signup(Request $request)
    {
        $rules=[
            'f_name' => ['required', 'string', 'min:1', 'max:50'],
            'l_name' => ['required', 'string', 'min:1', 'max:50'],
            'phone_number' => ['required', 'string', 'min:12', 'max:13'],
            'email' => ['required', 'string', 'email','unique:users'],
            'password' => ['required', 'string']
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return ResponseFactory::errorJson(null, $validator->errors());
        }

        $user = User::create([
            "f_name"=> $request->f_name,
            "l_name"=> $request->l_name,
            "phone_number"=> $request->phone_number,
            "email"=> $request->email,
            "password"=> $request->password,
            "active"=> 1,
        ]);

        return ResponseFactory::resJson($user, "User Created");
    }

    public function signout(Request $reqest)
    {
        // Get user who requested the logout
        $user = request()->user();

        // Revoke current user token
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return ResponseFactory::resJson(null, "See you Soon");
    }
}
