<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Exception;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function updateProfilePhoto(Request $request)
    {
    	try {
    		$url = $this->storePhoto();
	        $user=request()->user();

            $user->image()->create(['url' => $url]);
	        return response()->json(["message"=>"'Profile succesfully updated'","success"=>true]);
    	} catch (Exception $e) {
    		return response()->json(["error"=>true,"message"=>"An error ocurred.","code"=>$e->getMessage(),"data"=>null]);
    	}
    }

    public function updateProjectPhoto(Request $request)
    {
    	try {
    		$url = $this->storePhoto();
            $project = Project::find($request->file('file'));
            $project->image()->create(['url' => $url]);
	        return response()->json(["message"=>"'Profile succesfully updated'","success"=>true]);
    	} catch (Exception $e) {
    		return response()->json(["error"=>true,"message"=>"An error ocurred.","code"=>$e->getMessage(),"data"=>null]);
    	}
    }

    private function storePhoto(){
        $request = request();
        $data=$request->all();
        if ($request->hasFile('file')){
            $image = \Image::make($request->file('file'));
            $nombre = "file_" . uniqid() . ".jpg";
            $image->save(storage_path() . '/profile/' . $nombre, 100);
            $data["photo"]=$nombre;
        }else{
            if (isset($data["file"])&&$data["file"]) {
                $img = $data["file"];
                $partes = explode(",", $img);
                if (count($partes)){
                    $image=base64_decode($partes[1]);
                    $base64 = str_replace(";base64", "", $partes[0]);
                    $tipo = str_replace("data:image/", "", $base64);
                    $nombre = "file_" . uniqid() . "." . $tipo;
                    $img = str_replace(' ', '+', $partes[1]);
                    $img1 = substr($img, 1);
                    $image = \Image::make(base64_decode($partes[1]) . "," . $img1);
                    if (!file_exists(storage_path() . '/profile')) {
                        mkdir(storage_path() . '/profile', 666, true);
                    }
                    $image->save(storage_path() . '/profile/' . $nombre, 100);
                    $data["photo"]=$nombre;
                }
            }
        }
        return $data['photo'];
    }
}
