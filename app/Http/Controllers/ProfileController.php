<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

class ProfileController extends Controller
{
    public function index(Request $reqest)
    {
        $user = request()->user();
		return ResponseFactory::resJson($user, "Here You go");
    }

    public function updateProfilePhoto(Request $request)
    {
        $data=$request->all();
    	try {
    		if ($request->hasFile('file')){
	            $image = \Image::make($request->file('file'));
	            $nombre = "file_" . uniqid() . ".jpg";
	            $image->save(storage_path() . '/profile/' . $nombre, 100);
	            $data["photo"]=$nombre;
	        }else{
	            if (isset($data["file"])&&$data["file"]) {
	                $img = $data["file"];
	                $partes = explode(",", $img);
	                if (count($partes)){
	                    $image=base64_decode($partes[1]);
	                    $base64 = str_replace(";base64", "", $partes[0]);
	                    $tipo = str_replace("data:image/", "", $base64);
	                    $nombre = "file_" . uniqid() . "." . $tipo;
	                    $img = str_replace(' ', '+', $partes[1]);
	                    $img1 = substr($img, 1);
	                    $image = \Image::make(base64_decode($partes[1]) . "," . $img1);
                        if (!file_exists(storage_path() . '/profile')) {
                            mkdir(storage_path() . '/profile', 666, true);
                        }
	                    $image->save(storage_path() . '/profile/' . $nombre, 100);
	                    $data["photo"]=$nombre;
	                }
	            }
	        }
	        $user=request()->user();
	        $user->photo=$data["photo"];
	        $user->save();
			return ResponseFactory::resJson(null, 'Profile succesfully updated');
    	} catch (Exception $e) {
			return ResponseFactory::errorJson(null, $e->getMessage());
    	}
    }

	public function updateProfile(Request $request)
	{
		try {
			$user = request()->user();
			$user->f_name = $request->f_name;
			$user->l_name = $request->l_name;
			$user->email = $request->email;
			$user->phone_number = $request->phone_number;
			$user->address = $request->address;
			$user->city = $request->city;
			$user->country = $request->country;
			$user->postal = $request->postal;
			$user->about = $request->about;
			$user->save();
			return ResponseFactory::resJson($user, "updated Successfully");

		} catch (\Throwable $th) {
			return ResponseFactory::errorJson(null, "Something Went Wrong.");
		}
	}
}
