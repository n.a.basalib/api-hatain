<?php 

namespace App\Mixins;

use Illuminate\Support\Str;

class StrMixins
{
    public function phone_number()
    {
        return function ($phone){
            if(str::startsWith($phone, '05')){
                return '+966' . substr($phone, 1);
            }
            return $phone;
        };
    }
}