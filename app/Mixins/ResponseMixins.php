<?php 

namespace App\Mixins;

use Illuminate\Support\Str;

class ResponseMixins
{
    public function resJson()
    {
        return function ($data = null, $message = "Success"){
            return response()->json([
                "success" => true,
                "message" => $message,
                "data" => $data,
            ]);
        };
    }

    public function errorJson()
    {
        return function ($data = null, $message = "Invalid request"){
            return response()->json([
                "success" => false,
                "message" => $message,
                "data" => $data,
            ]);
        };
    }
}