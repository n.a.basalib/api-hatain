<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'logo',
        'iteration',
        'total',
        'payment_period',
        'fees',
        'status',
    ];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
