<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\QueryFilter\Active;
use Illuminate\Pipeline\Pipeline;
use App\QueryFilter\Sort;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'f_name',
        'l_name',
        'password',
        'phone_number',
        'email',
        'address',
        'city',
        'country',
        'postal_code',
        'about',
        'active',
        'remember_token '
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = [
        'photo',
        'photo_plain',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function newQuery($excludeDeleted = true){
    //     $raw='';
    //     $string=Str::random(30);
    //     DB::insert("insert into files_token (token)values(?);",[$string]);
    //     $raw .= "concat('".config("app.api_backend")."/img/profile/',photo,'?token=".$string."') as photo,photo as photo_plain ";
        
    //     return parent::newQuery($excludeDeleted)->addSelect('*', DB::raw($raw));
    // }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getPhotoAttribute(){
        $string=Str::random(30);
        DB::insert("insert into files_token (token)values(?);",[$string]);
        return config("app.api_backend")."/img/profile/" . $this->image()->pluck('url')->first() . "?token=".$string;
    }


    public function getPhotoPlainAttribute(){
        return $this->image()->pluck('url')->first();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public static function allUsers()
    {
        return app(Pipeline::class)
        ->send(User::query())
        ->through([
            Active::class,
            Sort::class
        ])
        ->thenReturn()
        ->get();
    }
}
