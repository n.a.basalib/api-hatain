<?php

namespace App\Billing;

interface PaymentMethodContract{

    public function setBalance($balance);

    public function charge($amount);
}