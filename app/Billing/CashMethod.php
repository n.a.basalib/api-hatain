<?php 

namespace App\Billing;

use Illuminate\Support\Str;


class CashMethod implements PaymentMethodContract{

    private $currency;
    private $balance;

    public function __construct($currency)
    {
        $this->currency = $currency;
        $this->balance = 0;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    public function charge($amount)
    {
        // charge the bank

        return [
            'amount' => $amount - $this->balance,
            'confimation_number' => Str::random(),
            'currency' => $this->currency,
            'balance' => $this->balance,
        ];
    }
}

