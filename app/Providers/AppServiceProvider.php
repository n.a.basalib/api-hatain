<?php

namespace App\Providers;

use App\Billing\BankMethod;
use App\Billing\CashMethod;
use App\Billing\PaymentMethodContract;
use App\SMS\SMSMessage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PaymentMethodContract::class, function($app){
            if(request()->has('credit'))
            return new BankMethod('SAR');
            return new CashMethod('SAR');
        });

        $this->app->singleton('SMS', function($app){
            return new SMSMessage('Hatain');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }
}
