<?php

namespace App\SMS;

class SMSMessage{

    private $name;
    private $phone_number;
    private $message;
    private $sender_name; 

    public function __construct($sender_name)
    {
        $this->sender_name = $sender_name;
    }

    public function sendPaymentConformation($name, $phone, $message = 'hello')
    {
        $this->name = $name;
        $this->phone_number = $phone;
        $this->message = $message;

        $this->send();
    }

    private function send(){
        // send sms with proprety of this class
        dd("the sender is " . $this->sender_name . " to " . $this->name . " on " . $this->phone_number . " with content " . $this->message);
    }
}