<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileToken extends Model
{
    protected $table="files_token";
	protected $fillable=["token"];
}
