<?php

use App\SMS\Facade\SMS;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/'], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::post('/login', 'LoginController@login');
        Route::post('/signup', 'LoginController@signup');
    });
});

Route::get("img/profile/{name}", function ($name, Request $request) {
    $file = storage_path() . '/profile/' . $name;
    $token = \App\FileToken::where("token", $request->token)->first();
    if (!$token) {
        return ["file not found"];
    }
    header('Content-Type: ' . mime_content_type($file));
    header('Content-Length: ' . filesize($file));
    echo file_get_contents($file);
});

Route::get('/sms', function(){
    SMS::sendPaymentConformation('naif', '966530757417', 'We receive the payment, Thank You');
});

Route::group(['prefix' => '/', 'middleware' => 'auth:sanctum'], function () {
// Route::group(['prefix' => '/'], function () {
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileController@index');
        Route::post('/update/photo', 'ImageController@updateProfilePhoto');
        Route::post('/update', 'ProfileController@updateProfile');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index');
        Route::post('/signout', 'LoginController@signout');
    });

    Route::group(['prefix' => 'project'], function () {
        Route::get('/', 'ProjectController@index');
        Route::post('/', 'ProjectController@store');
        Route::post('/update/photo', 'ImageController@updateprojectPhoto');
        Route::put('/{id}', 'ProjectController@update');
        Route::delete('/{id}', 'ProjectController@delete');
    });

    Route::group(['prefix' => 'payment'], function () {
       // Route::get('/', 'PaymentController@index');
        Route::get('/', 'PaymentController@store');
        Route::put('/{id}', 'PaymentController@update');
        Route::delete('/{id}', 'PaymentController@delete');
    });
});